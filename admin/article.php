<?php
if ($_SESSION['Auth']['role'] == 1) {

// SUPPRESSION
    if (isset($_GET['article_delete'])) {
        csrfVerify();
        $delete = $db->prepare("DELETE FROM blog_data WHERE blog_id=?");
        $delete->execute([$_GET['article_delete']]);
        Session::setFlash('Catégorie supprimée avec succès');
        header('Location:article.php');
        die();
    }
// AFFICHAGE
    $select = $db->query("SELECT * FROM blog_data ORDER BY blog_date DESC");
    $articles = $select->fetchAll();

    $titres = [
        '#',
        'Nom',
        'Date',
        '<div class="text-right">
            <a href="article_edit" class="btn btn-primary">Créer</a>
        </div>'
    ]

    ?>

    <h1 class="p-4 bg-primary text-white">Articles de Blog</h1>

    <table class="table table-hover">
        <thead class="thead-dark">
        <tr>
            <?php foreach ($titres as $titre) { ?>
                <th scope="col"><?php echo $titre ?></th>
            <?php } ?>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($articles as $article) { ?>
            <tr>
                <th scope="row"><?php echo $article['blog_id']; ?></th>
                <td><a href="<?php echo WEBROOT; ?>article/<?php echo $article['blog_slug']; ?>"><?php echo $article['blog_title']; ?></a></td>
                <td><?php echo $article['blog_date']; ?></td>
                <td>
                    <div class="text-right">
                        <a href="article_edit?blog_id=<?php echo $article['blog_id']; ?>" class="btn btn-primary">EDITER</a>
                        <a href="?article_delete=<?php echo $article['blog_id'] . '&' . csrf(); ?>" class="btn btn-danger">SUPPRIMER</a>
                    </div>
                </td>
            </tr>
        <?php } ?>
        <tr class="bg-dark">
            <td colspan="4">
                <div class="text-right">
                    <a href="article_edit" class="btn btn-primary">Créer</a>
                </div>
            </td>
        </tr>
        </tbody>
    </table>

<?php } else {
    header('Location:../');
} ?>