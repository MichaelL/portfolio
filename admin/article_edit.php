<?php
include '../controllers/includes.php';

if ($_SESSION['Auth']['role'] == 1) {

// ARTICLE
    if (isset($_POST['blog_title']) && isset($_POST['blog_description']) && isset($_POST['blog_date']) && isset($_POST['blog_content'])) {
        $aujourdhui = new DateTime();
        $date = new DateTime($_POST['blog_date']);
        $blog_img = $_FILES['blog_img'];
        $file_extension = pathinfo($blog_img['name'], PATHINFO_EXTENSION);

        // VALIDATION
        if ($_POST['blog_title'] == '') {
            Session::setFlash("Veuillez renseigner un <b>Titre</b>", 'danger');
        } elseif (!preg_match('/^[A-ZÀ-ÖØ-Ü][A-ZÀ-ÖØ-Üa-zà-öø-ÿ\h]+$/', $_POST['blog_title'])) {
            Session::setFlash("<b>Titre</b> invalide", 'danger');
        } elseif ($_POST['blog_description'] == '') {
            Session::setFlash("Veuillez renseigner une <b>Description</b>", 'danger');
        } elseif (!isset($_GET['blog_id']) && $blog_img['size'] == 0) {
            Session::setFlash("Veuillez insèrer une <b>Image</b>", 'danger');
        } elseif (!isset($_GET['blog_id']) && !in_array($file_extension, ['jpg', 'png'])) {
            Session::setFlash("Format d'<b>Image</b> valide : <span class='lead font-weight-bold'>.jpg</span> ou <span class='lead font-weight-bold'>.png</span> uniquement", 'danger');
        } elseif ($_POST['blog_date'] == '') {
            Session::setFlash("Veuillez renseigner une <b>Date</b>", 'danger');
        } elseif ($date > $aujourdhui) {
            Session::setFlash("La <b>Date</b> ne peut pas être supérieur à aujourdhui", 'danger');
        } elseif ($_POST['blog_content'] == '') {
            Session::setFlash("Champs <b>Contenu</b> vide", 'danger');
        } else {
            $blog_slug = Url::transform($_POST['blog_title']);

            // UPDATE
            if (isset($_GET['blog_id'])) {
                $update = $db->prepare("UPDATE blog_data SET blog_slug=?, blog_title=?, blog_date=?, blog_description=?, blog_content=?, category_id=? WHERE blog_id=?");
                $update->execute([$blog_slug, $_POST['blog_title'], $_POST['blog_date'], $_POST['blog_description'], $_POST['blog_content'], $_POST['category_id'], $_GET['blog_id']]);
                Session::setFlash('Article <b>'. $_POST['blog_title'].'</b> enregistrée');
                header('Location:article');
            } // CREATE
            else {
                $create = $db->prepare("INSERT INTO blog_data SET blog_slug=?, blog_title=?, blog_date=?, blog_description=?, blog_content=?, category_id=?");
                $create->execute([$blog_slug, $_POST['blog_title'], $_POST['blog_date'], $_POST['blog_description'], $_POST['blog_content'], $_POST['category_id']]);
                $_GET['blog_id'] = $db->lastInsertId();
                Session::setFlash('Article <b>'. $_POST['blog_title'].'</b> créée');
                header('Location:article');
            }

            // IMAGES

            // CREATE & UPDATE
            if (in_array($file_extension, ['jpg', 'png'])) {

                $img_id = $_GET['blog_id'];
                $img_name = $img_id . '.' . $file_extension;

                if (!file_exists(IMAGES . '/articles/' . $img_name)) {
                    $insert_img = $db->prepare("INSERT INTO images SET blog_id=?");
                    $insert_img->execute([$_GET['blog_id']]);


                    $db_img_id = $db->lastInsertId();

                    move_uploaded_file($blog_img['tmp_name'], IMAGES . '/articles/' . $img_name);

                    $update_img = $db->prepare("UPDATE images SET name=? WHERE id=?");
                    $update_img->execute([$img_name, $db_img_id]);
                    header('Location:article');
                } else {
                    unlink(IMAGES . '/articles/' . $img_name);
                    move_uploaded_file($blog_img['tmp_name'], IMAGES . '/articles/' . $img_name);
                    Session::setFlash("Image : $img_name, Enregistré !");
                }
            }
            die();
        }
    }

// READ
    if (isset($_GET['blog_id'])) {
        $title = 'Michaël LEMAY | Éditer un Article';
        $subtitle = "Éditer un article";
        $select = $db->prepare("SELECT * FROM blog_data WHERE blog_id=?");
        $select->execute([$_GET['blog_id']]);
        if ($select->rowCount() == 0) {
            Session::setFlash('Article inexistante', 'danger');
            header('Location:article');
            die();
        }
        $_POST = $select->fetch();
    } else {
        $title = 'Michaël LEMAY | Créer un Article';
        $subtitle = "Créer un article";
    }

// CATÉGORIES
    $select = $db->query("SELECT id, name FROM categories ORDER BY name ASC");
    $categories = $select->fetchAll();
    $categories_select = [];
    foreach ($categories as $category) {
        $categories_select[$category['id']] = $category['name'];
    }

// IMAGES
    if (isset($_GET['blog_id'])) {
        // AFFICHAGE
        $select = $db->prepare("SELECT id, name FROM images WHERE blog_id=?");
        $select->execute([$_GET['blog_id']]);
        $images = $select->fetchAll();
    } else {
        $images = [];
    }
// SUPPRESSION D'IMAGES
    if (isset($_GET['delete'])) {
        csrfVerify();
        $select = $db->prepare("SELECT name, blog_id FROM images WHERE id=?");
        $select->execute([$_GET['delete']]);
        $image = $select->fetch();
        unlink(IMAGES . '/articles/' . $image['name']);

        $delete = $db->prepare("DELETE FROM images WHERE id=?");
        $delete->execute([$_GET['delete']]);
        Session::setFlash('Image supprimée avec succès');
        header('Location:article_edit?blog_id=' . $image['blog_id']);
        die();
    }

    include '../includes/admin_header.php'; ?>

    <a href="<?php echo WEBROOT ?>admin/#article">Retour</a>

    <h1 class="p-4 bg-primary text-white"><?php echo $subtitle ?></h1>

    <form action="#" method="POST" class="row" enctype="multipart/form-data">
        <div class="form-group col-lg-6">
            <label for="blog_title">Titre :</label>
            <?php echo Form::input('blog_title'); ?>
        </div>
        <div class="form-group col-lg-6">
            <label for="blog_description">Description :</label>
            <?php echo Form::input('blog_description'); ?>
        </div>
        <div class="form-group col-lg-4">
            <label for="category_id">Catégorie :</label>
            <?php echo Form::select('category_id', $categories_select); ?>
        </div>
        <div class="form-group col-lg-4">
            <label for="blog_img">Image :</label>
            <?php echo Form::input('blog_img', 'file') ?>
        </div>
        <div class="form-group col-lg-4">
            <label for="blog_date">Date :</label>
            <?php echo Form::input('blog_date', 'date'); ?>
        </div>
        <div class="m-0 row col-lg-12">
            <?php foreach ($images as $key => $image) { ?>
                <div class="p-0 m-auto border-0 card col-lg-2">
                    <img src="<?php echo WEBROOT ?>assets/img/articles/<?php echo $image['name']; ?>"
                         class="img-thumbnail" alt="">
                    <a href="?delete=<?php echo $image['id'] . '&' . csrf(); ?>" class="btn btn-danger">SUPPRIMER</a>
                </div>
            <?php } ?>
        </div>
        <div class="form-group col-lg-12">
            <label for="blog_content">Contenu :</label>
            <?php echo Form::textarea('blog_content'); ?>
        </div>
        <div class="col text-center">
            <button type="submit" class="btn btn-success">Enregistrer</button>
        </div>
    </form>

    <?php ob_start() ?>
    <script src="<?php echo WEBROOT; ?>assets/js/tinymce/tinymce.min.js"></script>
    <script type="text/javascript">
        tinymce.init({
            selector: '.tinyMCE'
        });
    </script>
    <?php $script = ob_get_clean(); ?>

    <?php include '../includes/footer.php'; ?>

<?php } else {
    header('Location:'.WEBROOT);
} ?>
