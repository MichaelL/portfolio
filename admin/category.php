<?php
if ($_SESSION['Auth']['role'] == 1) {

// SUPPRESSION
    if (isset($_GET['category_delete'])) {
        csrfVerify();
        $delete = $db->prepare("DELETE FROM categories WHERE id=?");
        $delete->execute([$_GET['category_delete']]);
        Session::setFlash('Catégorie supprimée avec succès');
        header('Location:category.php');
        die();
    }
// AFFICHAGE
    $select = $db->query("SELECT id, name, slug FROM categories");
    $categories = $select->fetchAll();

    $titres = [
        '#',
        'Nom',
        '<div class="text-right">
            <a href="category_edit" class="btn btn-primary">Créer</a>
        </div>'
    ]

    ?>
    <h1 class="p-4 bg-info text-white">Catégories</h1>

    <table class="table table-hover">
        <thead class="thead-dark">
        <tr>
    <?php foreach ($titres as $titre) { ?>
            <th scope="col"><?php echo $titre ?></th>
    <?php } ?>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($categories as $category) { ?>
            <tr>
                <th scope="row"><?php echo $category['id']; ?></th>
                <td><a href="<?php echo WEBROOT; ?>categorie/<?php echo $category['slug'] ?>"><?php echo $category['name'] ?></a></td>
                <td>
                    <div class="text-right">
                        <a href="category_edit?id=<?php echo $category['id']; ?>" class="btn btn-primary">EDITER</a>
                        <a href="?category_delete=<?php echo $category['id'] . '&' . csrf(); ?>" class="btn btn-danger">SUPPRIMER</a>
                    </div>
                </td>
            </tr>
        <?php } ?>
        <tr class="bg-dark">
            <td colspan="3">
                <div class="text-right">
                    <a href="category_edit" class="btn btn-primary">Créer</a>
                </div>
            </td>
        </tr>
        </tbody>
    </table>

<?php } else {
    header('Location:../');
} ?>