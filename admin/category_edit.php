<?php
include '../controllers/includes.php';

if ($_SESSION['Auth']['role'] == 1) {

    if (isset($_POST['name'])) {
        if ($_POST['name'] == '') {
            Session::setFlash('Veuillez renseigner un <b>Nom</b>', 'danger');
        } elseif (!preg_match('/^[A-ZÀ-ÖØ-Ü][a-zà-öø-ÿ]+$/', $_POST['name'])) {
            Session::setFlash("<b>Nom</b> invalide", 'danger');
        } else {
            $slug = Url::transform($_POST['name']);

            if (isset($_GET['id'])) {
                $update = $db->prepare("UPDATE categories SET name=?, slug=? WHERE id=?");
                $update->execute([$_POST['name'], $slug, $_GET['id']]);
                Session::setFlash('Catégorie <b>'. $_POST['name'].'</b> enregistrée');
                header('Location:category');
            } else {
                $create = $db->prepare("INSERT INTO categories SET name=?, slug=?");
                $create->execute([$_POST['name'], $slug]);
                Session::setFlash('Catégorie <b>'. $_POST['name'].'</b> créée');
                header('Location:category');
            }
            die();
        }
    }

    if (isset($_GET['id'])) {
        $title = 'Michaël LEMAY | Éditer une Catégorie';
        $subtitle = "Éditer une catégorie";
        $select = $db->prepare("SELECT * FROM categories WHERE id=?");
        $select->execute([$_GET['id']]);
        if ($select->rowCount() == 0) {
            Session::setFlash('Catégorie inexistante', 'danger');
            header('Location:category');
            die();
        }
        $_POST = $select->fetch();
    } else {
        $title = 'Michaël LEMAY | Créer une Catégorie';
        $subtitle = "Créer une catégorie";
    }

    include '../includes/admin_header.php'; ?>

    <a href="<?php echo WEBROOT ?>admin/#category">Retour</a>

    <h1 class="p-4 bg-info text-white"><?php echo $subtitle ?></h1>

    <form action="#" method="POST" class="row">
        <div class="form-group col-lg-12">
            <label for="name">Nom :</label>
            <?php echo Form::input('name') ?>
        </div>
        <div class="col text-center">
            <button type="submit" class="btn btn-success">Enregistrer</button>
        </div>
    </form>

    <?php include '../includes/footer.php'; ?>

<?php } else {
    header('Location:'.WEBROOT);
} ?>
