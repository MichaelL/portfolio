<?php
$title = 'Michaël LEMAY | Admin';
include '../controllers/includes.php';

if ($_SESSION['Auth']['role'] == 1) {

    include '../includes/admin_header.php'; ?>

    <h1>Administration</h1>
    <h2>Quoi de neuf <?php echo $_SESSION['Auth']['fname'] ?> ?</h2>

    <div class="row mx-0 my-4">
        <div class="col text-center list-group" id="admin-navbar" role="tablist">
            <a class="btn btn-outline-info active" href="#category" id="category-list" data-toggle="list" role="tab">Catégories</a>
            <a class="btn btn-outline-primary" href="#article" id="article-list" data-toggle="list" role="tab">Articles</a>
            <a class="btn btn-outline-warning" href="#user" id="user-list" data-toggle="list" role="tab">Membres</a>
        </div>
    </div>
    <div class="col-lg-12">
        <div class="tab-content" id="nav-admin">
            <div class="tab-pane fade show active" id="category" role="tabpanel" aria-labelledby="category-list">
                <?php include 'category.php';?>
            </div>
            <div class="tab-pane fade" id="article" role="tabpanel" aria-labelledby="article-list">
                <?php include 'article.php';?>
            </div>
            <div class="tab-pane fade" id="user" role="tabpanel" aria-labelledby="user-list">
                <?php include 'user.php';?>
            </div>
        </div>
    </div>

    <?php include '../includes/footer.php'; ?>

<?php } else {
    header('Location:'.WEBROOT);
} ?>
