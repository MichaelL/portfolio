<?php
if ($_SESSION['Auth']['role'] == 1) {

// SUPPRESSION
    if (isset($_GET['user_delete'])) {
        csrfVerify();
        $delete = $db->prepare("DELETE FROM members WHERE id=?");
        $delete->execute([$_GET['user_delete']]);
        Session::setFlash('Utilisateur supprimée avec succès');
        header('Location:user');
        die();
    }
// AFFICHAGE
    $select = $db->query("SELECT id, fname, lname, email, role FROM members ORDER BY role DESC");
    $users = $select->fetchAll();

    $titres = [
            'Prénom',
        'Nom',
        'Email',
        'Role',
        ''
    ]

    ?>

    <h1 class="p-4 bg-warning">Membres</h1>
    <form class="table-responsive">
        <table class="table table-hover">
            <thead class="thead-dark">
            <tr>
                <?php foreach ($titres as $titre) { ?>
                    <th scope="col"><?php echo $titre ?></th>
                <?php } ?>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($users as $user) { ?>
                <tr>
                    <td><?php echo $user['fname']; ?></td>
                    <td><?php echo $user['lname']; ?></td>
                    <td><a href="mailto:<?php echo $user['email']; ?>"><?php echo $user['email']; ?></a></td>
                    <td><?php
                        if ($user['role'] == 1) {
                            echo 'Administrateur';
                        } elseif ($user['role'] == 0) {
                            echo 'Membre';
                        } else {
                            echo '<pre class="text-danger">ERROR!</pre>';
                        } ?></td>
                    <td>
                        <div class="text-right">
                            <a href="user_edit?id=<?php echo $user['id']; ?>" class="btn btn-primary">EDITER</a>
                            <a href="?user_delete=<?php echo $user['id'] . '&' . csrf(); ?>" class="btn btn-danger">SUPPRIMER</a>
                        </div>
                    </td>
                </tr>
            <?php } ?>
            <tr class="bg-dark">
                <td colspan="5"></td>
            </tr>
            </tbody>
        </table>
    </form>

<?php } else {
    header('Location:../');
} ?>