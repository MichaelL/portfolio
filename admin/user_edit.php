<?php
$title = 'Michaël LEMAY | Membre';
include '../controllers/includes.php';

if ($_SESSION['Auth']['role'] == 1) {

    if (isset($_POST['role'])) {
        if ($_POST['role'] == '') {
            Session::setFlash('Veuillez renseigner un <b>Role</b>', 'danger');
        } else {

//UPDATE
            if (isset($_GET['id'])) {
                $update = $db->prepare("UPDATE members SET role=? WHERE id=?");
                $update->execute([$_POST['role'], $_GET['id']]);
                Session::setFlash('Role du membre modifié avec succès');
                header('Location:user');
                die();
            }
            die();
        }
    }

    if (isset($_GET['id'])) {
        $select = $db->prepare("SELECT * FROM members WHERE id=?");
        $select->execute([$_GET['id']]);
        if ($select->rowCount() == 0) {
            Session::setFlash('Membre inexistante', 'danger');
            header('Location:user');
            die();
        }
        $_POST = $select->fetch();
        $subtitle = $_POST['fname'].' '.$_POST['lname'];
    }

    $categories_select = [1 => "Administrateur", 0 => "Membre"];


    include '../includes/admin_header.php'; ?>

    <a href="<?php echo WEBROOT ?>admin/#user">Retour</a>

    <h1 class="p-4 bg-warning"><?php echo $subtitle ?></h1>

    <form action="#" method="POST" class="row">
        <div class="col-lg-3"></div>
        <div class="form-group col-lg-3">
            <label for="name"><abbr title="Administrateur = 1 | Membre = 0">Role :</abbr></label>
            <?php echo Form::select('role', $categories_select); ?>
        </div>
        <div class="col-lg-3 text-right"> Email :
            <a href="mailto:<?php echo $_POST['email']; ?>"><?php echo $_POST['email']; ?></a>
        </div>
        <div class="col-lg-3"></div>
        <div class="col text-center">
            <button type="submit" class="btn btn-success">Enregistrer</button>
        </div>
    </form>

    <?php include '../includes/footer.php'; ?>

<?php } else {
    header('Location:' . WEBROOT);
} ?>
