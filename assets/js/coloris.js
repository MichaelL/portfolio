
const connection = $(".nav-link:contains('Connexion')"),
    deconnection = $(".nav-link:contains('Déconnexion')");

connection.css('color', 'var(--success)')
connection.css('text-shadow', '0 0 2px var(--success)')

deconnection.css('color', 'var(--danger)')
deconnection.css('text-shadow', '0 0 2px var(--danger)')
