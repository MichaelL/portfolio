$(function () {
    $('[data-toggle="popover"]').popover()
})

// MESSAGE DE SESSION
setTimeout("$('.alert').hide()", 8000)

// BOUTON scrollTop
const btn = $('#scrollTop');

$(window).scroll(function() {
    if ($(window).scrollTop() > 500) {
        btn.addClass('show');
    } else {
        btn.removeClass('show');
    }
});

btn.click( function(e) {
    e.preventDefault();
    $('html, body').animate({scrollTop:0}, '500');
});