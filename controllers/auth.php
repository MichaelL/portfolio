<?php
session_start();

if (!isset($auth)) {
    if(!isset($_SESSION['Auth']['id'])) {
        header('Location:' . WEBROOT . 'login');
        die();
    }
}

// CLÉ CSRF
if (!isset($_SESSION['csrf'])){
    $_SESSION['csrf'] = md5(time().rand(000001, 999999));
}

function csrf(){
    return 'csrf=' . $_SESSION['csrf'];
}

function csrfVerify(){
    if (!isset($_GET['csrf']) || $_GET['csrf'] != $_SESSION['csrf']){
        header('Location:' . WEBROOT . 'csrf.php');
        die();
    }
}