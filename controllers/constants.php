<?php
define('DIRECTORY', dirname(__FILE__, 2));

$dir = basename(DIRECTORY);
$url = explode($dir, $_SERVER['REQUEST_URI']);
if (count($url) == 1) {
    define('WEBROOT', DIRECTORY_SEPARATOR);
} else {
    define('WEBROOT', $url[0] . 'portfolio/');
}

define('IMAGES', DIRECTORY . DIRECTORY_SEPARATOR .'assets'. DIRECTORY_SEPARATOR .'img'. DIRECTORY_SEPARATOR);