<div class="debug mt-5">
    <div class="row">
        <div class="col-sm-4">
            <h2>SERVER</h2>
            <textarea class="form-control text-danger h-100" readonly>
                <?php var_dump($_SERVER); ?>
            </textarea>
        </div>
        <div class="col-sm-4">
            <h2>CONSTANTE</h2>
            <textarea class="form-control text-secondary h-100" readonly>
                <?php var_dump(get_defined_constants()); ?>
            </textarea>
        </div>
        <div class="col-sm-4">
            <h2>SESSION</h2>
            <?php if (isset($_SESSION["Auth"])) { ?>
                <pre class='text-center text-success'>Connecté</pre>
            <div class="table-responsive">
                <table class="table table-hover">
                    <thead class="thead-dark">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">csrf</th>
                        <th scope="col">Prénom</th>
                        <th scope="col">Nom</th>
                        <th scope="col">Email</th>
                        <th scope="col">role</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <th scope="row"><?php echo $_SESSION["Auth"]["id"]; ?></th>
                        <td><?php echo $_SESSION["csrf"]; ?></td>
                        <td><?php echo $_SESSION["Auth"]["fname"]; ?></td>
                        <td><?php echo $_SESSION["Auth"]["lname"]; ?></td>
                        <td><?php echo $_SESSION["Auth"]["email"]; ?></td>
                        <td><?php echo $_SESSION["Auth"]["role"]; ?></td>
                    </tr>
                    <tr class="bg-dark">
                        <td colspan="6"></td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <?php } else {
                echo "<pre class='text-center text-danger'>Déconnecté</pre>";
            } ?>
        </div>
    </div>
</div>