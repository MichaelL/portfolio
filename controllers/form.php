<?php
Class Form {
    /**
     * @param $id
     * @param string $type
     * @param string $options
     * @return string
     */
    public static function input($id, $type = 'text', $options = ''){
        $value = isset($_POST[$id]) ? $_POST[$id] : '';
        return "<input type='$type' class='form-control' id='$id' name='$id' value='$value' $options>";
    }

    /**
     * @param $id
     * @return string
     */
    public static function textarea($id){
        $value = isset($_POST[$id]) ? $_POST[$id] : '';
        return "<textarea class='form-control tinyMCE' id='$id' name='$id'>$value</textarea>";
    }

    /**
     * @param $id
     * @param array $options
     * @return string
     */
    public static function select($id, $options = []) {
        $return = "<select class='form-control' id='$id' name='$id'>";
        foreach ($options as $key => $value) {
            $selected = '';
            if (isset($_POST[$id]) && $key == $_POST[$id]) {
                $selected = 'selected="selected"';
            }
            $return .= "<option value='$key' $selected>$value</option>";
        }
        $return .= '</select>';
        return $return;
    }

}