<?php


class Url
{
    /**
     * Url constructor.
     * @param $text
     * @return string
     */
    public static function transform($text) {
        $utf8 = [
            '#à|á|â|ã|ä|å#' => 'a',
            '#À|Á|Â|Ã|Ä|Å#' => 'A',
            '#ì|í|î|ï#' => 'i',
            '#Ì|Í|Î|Ï#' => 'I',
            '#è|é|ê|ë#' => 'e',
            '#È|É|Ê|Ë#' => 'E',
            '#ð|ò|ó|ô|õ|ö#' => 'o',
            '#Ò|Ó|Ô|Õ|Ö#' => 'O',
            '#ù|ú|û|ü#' => 'u',
            '#Ù|Ú|Û|Ü#' => 'U',
            '#ý|ÿ#' => 'y',
            '#Ý#' => 'Y',
            '#ç#' => 'c',
            '#Ç#' => 'C',
            '#ñ#' => 'n',
            '#Ñ#' => 'N',
            '# #' => '-'
        ];
        return strtolower(preg_replace(array_keys($utf8), array_values($utf8), $text));
    }
}