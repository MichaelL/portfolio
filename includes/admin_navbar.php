<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <a class="navbar-brand" href="<?php echo WEBROOT ?>">LM</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarAdminContent"
            aria-controls="navbarAdminContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarAdminContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="<?php echo WEBROOT ?>">Accueil <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="adminDropdown" role="button" data-toggle="dropdown"
                   aria-haspopup="true" aria-expanded="false">
                    Administrer
                </a>
                <div class="dropdown-menu" aria-labelledby="adminDropdown">
                    <a class="dropdown-item bg-info text-white" href="category">Catégories</a>
                    <a class="dropdown-item bg-primary text-white" href="article">Articles</a>
                    <a class="dropdown-item bg-warning" href="user">Membres</a>
                </div>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?php echo WEBROOT ?>logout">Déconnexion</a>
            </li>
        </ul>
        <!--        <form class="form-inline my-2 my-lg-0">-->
        <!--            <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">-->
        <!--            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>-->
        <!--        </form>-->
    </div>
</nav>