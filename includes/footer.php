<style>
    .spinner {
        display: none;
    }
</style>
<?php //include DIRECTORY.'/controllers/debug.php'?>
            </div>
        </main>

        <footer class="mt-auto py-3">
            <div class="container">
                <span>© LEMAY Michaël - Tous droits réservés</span>
            </div>
        </footer>

        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="<?php echo WEBROOT ?>assets/js/bootstrap.min.js"></script>
        <?php if (isset($script)) { echo $script; } ?>
        <script src="<?php echo WEBROOT ?>assets/js/coloris.js" async></script>
        <script src="<?php echo WEBROOT ?>assets/js/main.js" async></script>
    </body>
</html>