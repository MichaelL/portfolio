<!doctype html>
<html class="h-100" lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo WEBROOT ?>assets/img/coffee-cup.ico"/>

    <title><?php echo $title ?></title>
    <link rel="stylesheet" href="<?php echo WEBROOT ?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo WEBROOT ?>assets/css/style.css">
</head>
<body class="d-flex flex-column h-100">
<script src="https://code.jquery.com/jquery-3.5.1.min.js"
        integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
        crossorigin="anonymous"></script>
<header>
    <?php include 'includes/navbar.php'; ?>
</header>

<a id="scrollTop"></a>
<main class="flex-shrink-0" role="main">
    <?php
    if ($_SERVER['REQUEST_URI'] == WEBROOT || $_SERVER['REQUEST_URI'] == WEBROOT . 'index.php') {
        ?>
        <style>
            main > .container {
                margin-top: -110px !important;
            }

            .alert {
                position: fixed;
                top: 60px;
                right: 5px;
            }
        </style>
        <video autoplay loop width="100%">

            <source src="assets/video/CoffeeCup.mp4"
                    type="video/mp4">

            Sorry, your browser doesn't support embedded videos.
        </video>

        <?php
    }
    ?>
    <div class="container my-5">

<?php echo Session::flash(); ?>