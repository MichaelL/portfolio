<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <a class="navbar-brand" href="<?php echo WEBROOT ?>">LM</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="<?php echo WEBROOT ?>">Accueil <span class="sr-only">(current)</span></a>
            </li>
            <?php if (!isset($_SESSION['Auth'])){?>
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo WEBROOT ?>login">Connexion</a>
                </li>
            <?php } else{ ?>
                <?php if ($_SESSION['Auth']['role'] == 1){?>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo WEBROOT ?>admin/">Administrer</a>
                    </li>
                <?php } ?>
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo WEBROOT ?>logout">Déconnexion</a>
                </li>
            <?php } ?>
        </ul>
<!--        --><?php
//        if (isset($_GET['search'])) {
//            $select = $db->prepare("SELECT * FROM blog_data WHERE blog_title LIKE ?");
//            $select->execute(['%'.$_GET['search'].'%']);
//            $articles = $select->fetchAll();
//        }
//        ?>
<!--        <form method="GET" class="form-inline my-2 my-lg-0">-->
<!--            <input class="form-control mr-sm-2" type="search" id="search" name="search" placeholder="Recherche" aria-label="Recherche">-->
<!--            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>-->
<!--        </form>-->
    </div>
</nav>