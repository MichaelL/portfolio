<?php
$auth = 0;
$title = 'Michaël LEMAY';
include 'controllers/includes.php';

$condition = '';
$category = false;
if (isset($_GET['category'])) {
    $select = $db->prepare("SELECT * FROM categories WHERE slug=?");
    $select->execute([$_GET['category']]);
    if ($select->rowCount() == 0) {
        header("HTTP/1.1 301 Moved Permanently");
        header("Location:".WEBROOT);
        die();
    }
    $category = $select->fetch();
    $condition = " WHERE blog_data.category_id={$category['id']}";
}
$index = '';
if ($_SERVER['REQUEST_URI'] == WEBROOT || $_SERVER['REQUEST_URI'] == WEBROOT . 'index.php') {
    $index = " ORDER BY blog_data.blog_date DESC LIMIT 0,3";
}

$articles = $db->query("SELECT blog_data.blog_id, blog_data.blog_slug, blog_data.blog_title, blog_data.blog_date, blog_data.blog_description, images.name as image_name
FROM blog_data
LEFT JOIN images ON images.blog_id = blog_data.blog_id
$condition
$index")->fetchAll();

$categories = $db->query("SELECT * FROM categories")->fetchAll();

include 'includes/header.php';
?>

<?php
if (isset($_SESSION['Auth'])) { ?>

    <?php if ($_SERVER['REQUEST_URI'] === '/portfolio/' || $_SERVER['REQUEST_URI'] === WEBROOT . 'index.php') {
        include 'includes/music.html';
    }
    if ($category) {
        $category_title = '<h1>Catégorie : '.$category['name'].'</h1>' ?>
    <?php } else {
        $category_title = '<div class="defilement"><h1>Bon retour parmis nous '.$_SESSION['Auth']['fname']. ' ! </h1></div>';
    } ?>

    <div class="row">

        <div class="col-lg-2">
            <div class="categories">
            <ul class="list-group">
                <li class="list-group-item">
                    Catégories
                </li>
                <?php foreach ($categories as $category) { ?>
                    <li class="list-group-item">
                        <a href="<?php echo WEBROOT; ?>categorie/<?php echo $category['slug'] ?>"><?php echo $category['name'] ?></a>
                    </li>
                <?php } ?>
            </ul>
            </div>
        </div>

        <div class="col-lg-10">

                <?php echo $category_title ?>

            <div class="row">
                <?php foreach ($articles as $key => $article) { ?>
                    <div class="p-2 col-lg-4">
                        <div class="p-0 card">
                            <a href="<?php echo WEBROOT; ?>article/<?php echo $article['blog_slug']; ?>">
                                <img src="<?php echo WEBROOT; ?>assets/img/articles/<?php echo $article['image_name']; ?>"
                                     alt="">
                            </a>
                            <small class="px-4 date"><?php echo $article['blog_date']; ?></small>
                            <div class="p-2">
                                <h2 class="text-center lead mb-4"><?php echo $article['blog_title']; ?></h2>
                                <p><?php echo $article['blog_description']; ?></p>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>

        </div>

    </div>

<?php } else { ?>
    <h2 class="inscription">Pas encore membre ?
        <a class="nav-link" href="<?php echo WEBROOT ?>login">Se Connecter</a>
    </h2>

<?php }
include 'includes/footer.php'; ?>