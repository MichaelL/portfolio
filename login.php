<?php
$auth = 0;
$title = 'Michaël LEMAY | Connexion';
include 'controllers/includes.php';

if (isset($_POST['email']) && isset($_POST['password'])) {

    if ($_POST['email'] == '') {
        Session::setFlash('Renseignez un <b>email</b>', 'danger');
    } elseif ($_POST['password'] == '') {
        Session::setFlash('Renseignez un <b>Mot de passe</b>', 'danger');
    } else {
        $select = $db->prepare("SELECT * FROM members WHERE email=?");
        $select->execute([$_POST['email']]);
        $user = $select->fetch(PDO::FETCH_ASSOC);
        if ($user) {
//            var_dump(password_hash($_POST['password'], PASSWORD_BCRYPT, ['cost' => 12]));
            if (password_verify($_POST['password'], $user['password'])) {

                $_SESSION['Auth'] = $user;
                Session::setFlash('Vous êtes désormais connecté');
                if ($_SESSION['Auth']['role'] == 1) {
                    header('Location:'.WEBROOT.'admin/');
                } else {
                    header('Location:'.WEBROOT);
                }
                die();
            } else {
                Session::setFlash('<b>Mot de passe</b> incorrect', 'danger');
            }

        } else {
            Session::setFlash('<b>Email</b> incorrect', 'danger');
        }
    }
}

include 'includes/header.php';
?>

    <form action="" method="POST">
        <div class="form-group">
            <label for="email">Adresse Email</label>
            <?php echo Form::input('email', 'email') ?>
        </div>
        <div class="form-group">
            <label for="password">Mot de passe</label>
            <?php echo Form::input('password', 'password') ?>
        </div>
        <div class="text-center">
            <button type="submit" class="btn btn-primary">Se connecter</button>
        </div>
    </form>
    <h6 class="mt-2 text-center">Pas encore membre ?
        <a class="nav-link" href="<?php echo WEBROOT ?>register">S'incrire</a>
    </h6>

<?php include 'includes/footer.php'; ?>