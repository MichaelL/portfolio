<?php
$auth = 0;
$title = 'Michaël LEMAY | Connexion';
include 'controllers/includes.php';

if (isset($_POST['email']) && isset($_POST['password'])) {
    $select = $db->prepare("SELECT 1 FROM members WHERE email=?");
    $select->execute([$_POST['email']]);
    $email = $select->fetch();

    if ($_POST['fname'] == '') {
        Session::setFlash('Veuillez indiquer votre <b>Prénom</b>', 'danger');
    } elseif ($_POST['lname'] == '') {
        Session::setFlash('Veuillez indiquer votre <b>Nom</b>', 'danger');
    } elseif (filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) === false) {
        Session::setFlash('Veuillez indiquer votre <b>Adresse Email</b>', 'danger');
    } elseif ($_POST['email'] == '') {
        Session::setFlash('Veuillez indiquer votre <b>Adresse Email</b>', 'danger');
    } elseif ($email) {
        Session::setFlash('Cette <b>Adresse Email</b> est déjà utilisé', 'danger');
    } elseif ($_POST['password'] == '') {
        Session::setFlash('Veuillez renseigner un <b>Mot de passe</b>', 'danger');
    } else {
        $insert = $db->prepare("INSERT INTO members SET fname=?, lname=?, email=?, password=?, role=?");
        $password = password_hash($_POST['password'], PASSWORD_BCRYPT, ['cost' => 12]);

        $insert->execute([$_POST['fname'],$_POST['lname'],$_POST['email'],$password,0]);
        Session::setFlash('Vous pouvez désormais vous <b>connecter</b>');
        header('Location:login');
        exit();
    }
}

include 'includes/header.php';
?>

    <form action="" method="POST">
        <div class="form-group">
            <label for="fname">Prénom</label>
            <?php echo Form::input('fname', 'text') ?>
        </div>
        <div class="form-group">
            <label for="lname">Nom</label>
            <?php echo Form::input('lname', 'text') ?>
        </div>
        <div class="form-group">
            <label for="email">Adresse Email</label>
            <?php echo Form::input('email', 'email') ?>
        </div>
        <div class="form-group">
            <label for="password">Mot de passe</label>
            <?php echo Form::input('password', 'password') ?>
        </div>
        <div class="text-center">
            <button type="submit" class="btn btn-primary">S'inscrire</button>
        </div>
    </form>
    <h6 class="mt-2 text-center">Déjà membre ?
        <a class="nav-link" href="<?php echo WEBROOT ?>login">Se Connecter</a>
    </h6>
<?php include 'includes/footer.php'; ?>