<?php
$auth = 0;
$title = 'Michaël LEMAY';
include 'controllers/includes.php';

if (!isset($_GET['slug'])) {
    header("HTTP/1.1 301 Moved Permanently");
    header("Location:".WEBROOT);
    die();
}
$select = $db->prepare("SELECT blog_id, blog_slug, blog_title, blog_date, blog_content, category_id
FROM blog_data WHERE blog_slug=?");
$select->execute([$_GET['slug']]);
if ($select->rowCount() == 0) {
    header("HTTP/1.1 301 Moved Permanently");
    header("Location:".WEBROOT);
    die();
}
$article = $select->fetch();
$article_id = $article['blog_id'];

$select = $db->prepare("SELECT * FROM images WHERE blog_id=?");
$select->execute([$article_id]);
$image = $select->fetch();

$category_id = $article['category_id'];

$select = $db->prepare("SELECT * FROM categories WHERE id=?");
$select->execute([$category_id]);
$category = $select->fetch();

include 'includes/header.php'; ?>
    <div>
        <a href="<?php echo WEBROOT; ?>categorie/<?php echo $category['slug'] ?>" class="badge badge-dark"><?php echo $category['name'] ?></a>
    </div>

    <div class="d-flex justify-content-lg-around">
        <h1><?php echo $article['blog_title'] ?></h1>
        <p class="text-muted"><?php echo $article['blog_date']; ?></p>
    </div>

    <div class="row">
        <div class="p-2 col-lg-12 text-center">
            <img src="<?php echo WEBROOT; ?>assets/img/articles/<?php echo $image['name']; ?>" class="w-100" alt="">
            <p class="py-2 px-4"><?php echo $article['blog_content']; ?></p>
        </div>
    </div>
<?php include 'includes/footer.php'; ?>